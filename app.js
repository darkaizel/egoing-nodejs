var express = require('express');
var app = express();

// jode template code pretty
app.locals.pretty = true;
// view engine을 설정 template
app.set('view engine', 'jade');
// view resource 설정, default: './views'
app.set('views', './views');
// resource 폴더를 설정
app.use(express.static('public'));


// router 설정(controller)
app.get('/template', (req, res) => {
	// template사용으로 render 사용
	res.render('temp', {time: Date(), _title: 'new title'});
});

app.get('/', (req, res) => {
	res.send('Hello World!');
});

app.get('/img', (req, res) => {
	res.send(`Hello img, <img src='123.gif'>`);
});

app.get('/dynamic', (req, res) => {
	var lis = '';
	for(var i = 0; i < 5; i++) {
		lis = lis + '<li> this is dynamic </li>';
	}
	var output = `
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<title>static page</title>
		</head>
		<body>
			<h1>Hello dynamic!</h1>
			<ul>${lis}</ul>
		</body>
	</html>
	`;
	res.send(output);
})

app.get('/login', (req, res) => {
	res.send('<h1>Login please</h1>');
});

app.listen(3000, () => {
	console.log('Example App Listening on port 3000');
});

// app.get('/', function(req, res) {
// 	res.send('Hello World!');
// });

// app.listen(3000, function() {
// 	console.log('Example app listening on port 3000');
// });
