var fs = require('fs');
var fname = 'data.txt';

// Sync
console.log('1 Sync Result');
var data = fs.readFileSync(fname, {encoding: 'UTF8'});
console.log(data);

// Async
console.log('2 Async Result');
fs.readFile(fname, {encoding: 'UTF8'}, function(err, data){
	console.log(3);
	console.log(data);
});
console.log(4);
